# No 1
Program video processing yang dibuat yaitu program sederhana Real-Time Color Detection. Program ini memiliki 2 fitur utama yaitu mengompresi file video dan mendeteksi warna melalui video web cam secara real-time. Program akan mengompresi video yang diunggah dengan mengubah resolusinya menjadi tinggi 360 piksel. Video yang telah dikompresi akan disimpan dalam folder "compressed" dengan nama file "compressed_video.mp4". Program ini menggunakan kode HEX warna dalam mendeteksi nilai warna yang ditangkap oleh kamera pada satu titik tepat di tengah-tengah layar.

Program ini menggunakan 3 library, di antaranya:
1.	Cv2 atau opencv berfungsi untuk pengolahan citra atau gambar dan video.
2.	Moviepy.editor berfungsi untuk memuat dan memanipulasi video.
3.	Os yaitu library yang berfungsi untuk memanipulasi dan menampilkan informasi file di dalam direktori sebuah operasi sistem.

Berikut adalah list use case dari program ini:
1.	Pengguna dapat menggunakan program ini untuk mengompresi video dengan resolusi tinggi menjadi video dengan resolusi yang lebih rendah (360 piksel tinggi). Ini berguna ketika pengguna ingin mengurangi ukuran file video agar lebih mudah untuk dibagikan melalui email atau media sosial.
2.	Program ini dapat digunakan sebagai alat pembelajaran atau demonstrasi dalam pengolahan video dan pengenalan warna menggunakan Python dan OpenCV. Para pengguna dapat mempelajari cara mengompresi video, mengakses webcam, dan mendeteksi warna dalam video secara real-time.

# No 2
Produk ini terdiri dari dua fitur utama: kompresi video dan deteksi warna menggunakan webcam. Produk ini dibuat dengan algoritma sederhana.

Berikut adalah penjelasan mengenai proses dan algoritma yang digunakan dalam setiap fitur:
1. Kompresi Video:
    a. Pengguna memasukkan path file video yang ingin dikompresi.
    b. Menggunakan library moviepy.editor, video input dibuka sebagai objek VideoFileClip.
    c. Video tersebut kemudian dikompresi dengan menggunakan metode resize dengan mengubah resolusi menjadi tinggi 360 piksel.
    d. Video yang telah dikompresi disimpan dalam file output dengan menggunakan metode write_videofile.
    e. Ukuran awal dan akhir file video dihitung menggunakan fungsi get_file_size.
    f. Hasil ukuran awal dan akhir file dicetak.

2. Deteksi Warna:
    a. Menggunakan library cv2, webcam diaktifkan dengan menggunakan objek VideoCapture.
    b. Set resolusi frame yang diambil dari webcam menjadi 1280x720 piksel menggunakan cap.set().
    c. Di dalam loop while, setiap frame dari webcam dibaca menggunakan cap.read().
    d. Frame tersebut kemudian dikonversi ke format HSV menggunakan cv2.cvtColor.
    e. Lokasi piksel tengah dihitung berdasarkan tinggi dan lebar frame.
    f. Nilai hue dari piksel tengah diambil dari frame HSV.
    g. Berdasarkan nilai hue tersebut, warna yang sesuai ditentukan dan disimpan dalam variabel color.
    h. Informasi warna ditampilkan di atas frame menggunakan cv2.putText.
    i. Simbol lingkaran ditampilkan di lokasi piksel tengah menggunakan cv2.circle.
    j. Frame yang telah diedit ditampilkan dalam jendela dengan cv2.imshow.
    k. Loop berlanjut hingga tombol Esc (kode ASCII 27) ditekan.

Secara keseluruhan, algoritma yang digunakan dalam produk ini memanfaatkan kemampuan library moviepy.editor untuk kompresi video dan library cv2 untuk mengakses webcam dan melakukan deteksi warna dalam video secara real-time.

# No 3
Demo real-time color detection:
![rt-color-detection](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h-uas/-/raw/main/demo-real-time-color-detection.gif)

[Link Source Code](https://github.com/nafakhairunnisa/real-time-color-detection)

# No 4
[Link Youtube](https://youtu.be/zRZtIyyXmbg)

# No 5
Konsep Video Processing:
Video processing pada program ini melibatkan pengolahan dan transformasi data video, seperti pemrosesan video real-time yang bertujuan untuk mendeteksi warna.

Konsep Video Compression:
Video compression adalah proses mengurangi ukuran file video dengan cara menghilangkan redundansi dan mengompresi data video sehingga dapat disimpan dengan mempertahankan kualitas visual video seoptimal mungkin sambil mengurangi ukuran file yang dihasilkan. Pada program ini mengompresi file video dengan algoritma sederhana dimana resolusi tinggi diubah menjadi 360 piksel.

Pengembangan Alat Video Processing:
Kode program yang diberikan merupakan contoh pengembangan alat video processing sederhana. Dalam kode tersebut, terdapat dua fitur utama: kompresi video dan deteksi warna menggunakan webcam.

# No 6
Program yang dibuat merupakan sebuah produk yang melakukan prediksi kelas (class) dari gambar menggunakan model machine learning yang telah dilatih sebelumnya. Produk ini menggunakan model deep learning CNN yang telah dilatih untuk mengklasifikasikan gambar antara "normal" dan "pothole" (lubang jalan). Program ini bertujuan untuk mendeteksi adanya pothole padList Use Case:

Program ini menggunakan 5 library, di antaranya:
1.	Cv2 atau opencv berfungsi untuk pengolahan citra atau gambar dan video.
2.	Numpy berfungsi untuk melakukan operasi matematika dan manipulasi data.
3.	Load_model dari tensorflow.keras.models berfungsi untuk memuat model yang telah dilatih sebelumnya dengan TensorFlow Keras.
4. Load_img dari tensorflow.keras.preprocessing.image untuk memuat gambar dari file ke dalam objek gambar yang dapat digunakan dalam TensorFlow Keras.
5. Img_to_array dari tensorflow.keras.preprocessing.image untuk mengonversi objek gambar menjadi array numpy. 
6. Preprocess_input dari tensorflow.keras.applications.vgg16 untuk memproses array gambar sebelum disiapkan sebagai input untuk model VGG16.

Berikut adalah list use case dari program ini:
1. Deteksi Lubang Jalan. Produk ini dapat digunakan dalam aplikasi untuk mendeteksi lubang jalan pada gambar. Dengan memberikan gambar jalan sebagai input, produk ini akan melakukan prediksi apakah gambar tersebut mengandung lubang jalan atau tidak. Ini dapat membantu dalam pemeliharaan jalan dan perbaikan infrastruktur.
2. Pengujian Kualitas Jalan. Produk ini dapat digunakan dalam aplikasi yang menguji kualitas jalan. Dengan memberikan gambar jalan yang diambil secara real-time, produk ini dapat memberikan prediksi apakah ada lubang jalan atau tidak. Ini dapat membantu pemerintah atau lembaga terkait dalam memantau dan memperbaiki jalan yang rusak.
3. Pengembangan Produk Terkait Pendeteksi Objek. Produk ini dapat digunakan sebagai salah satu komponen dalam pengembangan sistem yang lebih kompleks, seperti sistem pendeteksi objek yang lebih umum.

# No 7
Demo pothole detection:
![pothole-detection](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h-uas/-/raw/main/demo-pothole-detection.gif)

[Link Source Code](https://github.com/nafakhairunnisa/pothole-detection-python)

# No 8
[Link Youtube](https://youtu.be/oSFXfL2U5gA)

# No 9
Research & development dilakukan untuk mendeteksi lubang di jalan melalui gambar. Tujuannya untuk mencegah kecelakaan di jalan yang dilalui melalui gambar dari jalan tersebut. Berikut proses riset dan pengembangan model secara detail:
1. Data understanding: Data yang dikumpulkan berupa dataset gambar yang memuat penampakan jalan berlubang dan jalan normal yang nantinya akan digunakan pada modeling dan model evaluation. Dataset yang diambil berasal dari Kaggle dengan ukuran 203 MB yang di dalamnya terdapat gambar jalan normal dan gambar jalan berlubang.

2. Data preparation: Dataset yang diambil dari Kaggle memiliki 681 gambar yang sudah diberi label berdasarkan folder. Di dalamnya ada folder “normal” yang berisi 352 gambar jalan normal dan folder “potholes” yang berisi 329 gambar jalan berlubang. Tidak ada missing value maupun kendala lainnya sehingga dataset bisa langsung diproses.

3. Modeling: Dataset yang sudah tersedia dan sudah melalui data preparation kemudian dibagi menjadi dua yaitu data latih dan data uji. Sebanyak 75% dataset dijadikan sebagai data latih dan 25% dijadikan data uji. Model yang dibuat menggunakan deep learning Convolutional Neural Network (CNN). Algoritma ini dirancang khusus untuk mengenali pola dan fitur visual dalam gambar. Pemodelan dilakukan dengan nilai epoch sebanyak 25 untuk mendapatkan akurasi yang tinggi. Didapatkan hasil akurasi 86.47%.

4. Model Evaluation: Model yang sudah dibuat tadi diuji dengan confusion matrix. Dari keseluruhan 170 data uji, model berhasil memprediksi benar 147 data uji.

[Link Google Colab](https://colab.research.google.com/drive/1CE-iFMpCGDGU8ZN-q7D0wJfXtE_ipUls?usp=sharing)
[Link dataset](https://www.kaggle.com/datasets/atulyakumar98/pothole-detection-dataset)

# No 10
[Link Draft Paper](https://drive.google.com/drive/folders/15d95kPjYd_dF7yqJIAaXzBhfmzJgPYZU?usp=sharing)
